FROM python:3.9.1-alpine3.12

RUN pip install flask flask-cors
ENV PORT=8080 \
    FLASK_APP="/app.py" \
    FLASK_RUN_HOST="0.0.0.0" \
    FLASK_RUN_PORT=8080
EXPOSE $PORT

COPY . .
USER root


# TBD 
#RUN chgrp -R 0 .  && chmod -R g+rwx .
#USER 1001

CMD ["sh","-c", "flask run" ]

