from flask import Flask
from flask_cors import CORS
import os
app = Flask(__name__)

CORS(app)
CORS(app, resources={r"/*": {"origins": "*"}})

@app.route('/')
def hello_world():
    return os.getenv('message')

